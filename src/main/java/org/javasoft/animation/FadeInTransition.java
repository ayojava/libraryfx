package org.javasoft.animation;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.util.Duration;

public class FadeInTransition extends CachedTimelineAnimation{

    public FadeInTransition(Node node) {
        super(node,new Timeline(new KeyFrame(Duration.millis(0), new KeyValue(node.opacityProperty(), 0, WEB_EASE), new KeyValue(node.translateXProperty(), 100, WEB_EASE)),
                new KeyFrame(Duration.millis(500), new KeyValue(node.opacityProperty(), 1, WEB_EASE), new KeyValue(node.translateXProperty(), 0, WEB_EASE))));
        setCycleDuration(Duration.seconds(2));
        setDelay(Duration.seconds(0));
    }
}
