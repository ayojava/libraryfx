package org.javasoft;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.javasoft.constants.ViewIntf;
import org.javasoft.util.AlertUtil;
import org.javasoft.util.ImageUtil;

import java.util.Locale;
import java.util.ResourceBundle;

import static org.javasoft.constants.ViewIntf.DEFAULT_LOCALE;

/**
 * JavaFX App
 */
public class LibraryApp extends Application {

    private double x;

    private double y;

    private ImageUtil imageUtilInstance;

    private DbInit dbInit;

    public LibraryApp(){
        initApp();
    }

    @Override
    public void start(Stage stage) throws Exception {
        try{
            //Parent root = FXMLLoader.load(getClass().getResource(ViewIntf.LOGIN));
            //FXMLLoader loader = new FXMLLoader(getClass().getResource(ViewIntf.HOME));
            FXMLLoader loader = new FXMLLoader(getClass().getResource(ViewIntf.LOGIN));
            loader.setResources(ResourceBundle.getBundle("libraryfx", new Locale(DEFAULT_LOCALE)));

            Parent root = loader.load();
            stage.setTitle("Login");
            stage.initStyle(StageStyle.UNDECORATED);
            stage.initStyle(StageStyle.TRANSPARENT);
            root.setOnMousePressed(event -> {
                x=event.getSceneX();
                y=event.getSceneY();
            });
            root.setOnMouseDragged(event -> {
                stage.setX(event.getScreenX()-x);
                stage.setY(event.getScreenY()-y);
            });

            Scene scene = new Scene(root);
            scene.setFill(javafx.scene.paint.Color.TRANSPARENT);
            stage.getIcons().add(imageUtilInstance.getApplicationIcon());
            stage.centerOnScreen();
            stage.setScene(scene);
            stage.show();
        }catch (Exception exception) {
            AlertUtil.showExceptionDialog(exception);
        }
    }

    private void initApp(){
        imageUtilInstance = ImageUtil.getImageUtilInstance();
        dbInit = new DbInit();
        try {
           //dbInit.populateDB();
        } catch (Exception exception) {
            AlertUtil.showExceptionDialog(exception);
        }
    }



}


//https://github.com/ivandzf/YourCompanyJavaFX
//https://www.youtube.com/watch?v=nJ1bVyZx8Qc&t=239s
//https://www.youtube.com/watch?v=SeaF3lTmbQE
//https://www.youtube.com/watch?v=wIuqKYhGaa8
//https://www.youtube.com/watch?v=vciynijeKwI
//https://www.youtube.com/watch?v=CGWRwpeihE8
//https://github.com/yaw/javafx-jpa-crud
//https://github.com/MainDuel/Basic-JavaFX-CRUD-with-JPA-and-Hibernate

//    private static Scene scene;
//
//    @Override
//    public void start(Stage stage) throws IOException {
//        scene = new Scene(loadFXML("primary"), 640, 480);
//        stage.setScene(scene);
//        stage.show();
//    }
//
//    static void setRoot(String fxml) throws IOException {
//        scene.setRoot(loadFXML(fxml));
//    }
//
//    private static Parent loadFXML(String fxml) throws IOException {
//        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
//        return fxmlLoader.load();
//    }
//
//    public static void main(String[] args) {
//        launch();
//    }