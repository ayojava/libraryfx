package org.javasoft.download;

import javafx.scene.control.Alert;
import lombok.val;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.javasoft.entity.BookEntity;

import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import static org.javasoft.util.AlertUtil.showAlert;

public class BookExcelDownload {

    private String[] downloadColumns = {"No","Book Name","Book Code","Book Author " ,"Publisher Name","Book ISBN" ,"Genre" ,"Publisher Date" ,"Registered Date ","Status"};

    private String fileName="BookList";

    public void generateExcelSheet(List<BookEntity> bookEntityList){

        final XSSFWorkbook workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet(fileName);

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.BLUE.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Create cells
        for(int i = 0; i < downloadColumns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(downloadColumns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 , serialNo = 1;

        for(BookEntity bookEntity : bookEntityList){
            Row row = sheet.createRow(rowNum);
            row.createCell(0).setCellValue(rowNum);

            row.createCell(1).setCellValue(bookEntity.getBookName());
            row.createCell(2).setCellValue(bookEntity.getBookCode());
            row.createCell(3).setCellValue(bookEntity.getBookAuthor());
            row.createCell(4).setCellValue(bookEntity.getPublisherName());
            row.createCell(5).setCellValue(bookEntity.getBookISBN());
            row.createCell(6).setCellValue(bookEntity.getGenre());
            row.createCell(7).setCellValue(bookEntity.getPublisherDate().toString());
            row.createCell(8).setCellValue(bookEntity.getRegisteredDate().toString());
            row.createCell(9).setCellValue(bookEntity.getStatus());
            rowNum++;
            serialNo++;
        }

        for(int i = 0; i < downloadColumns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        val excelFileName = fileName + "-" + DateFormatUtils.format(new Date(),"dd-MM-yyyy") + ".xlsx";

        try{
            FileOutputStream fileOut = new FileOutputStream(excelFileName);
            workbook.write(fileOut);
            fileOut.close();

            showAlert(Alert.AlertType.INFORMATION,ResourceBundle.getBundle("libraryfx").getString("dialog.successLbl"),
                    "",ResourceBundle.getBundle("libraryfx").getString("dialog.exportSuccessLbl"));
        }catch (Exception exception){
            System.out.println("Exception " + exception);
        }


    }

}
