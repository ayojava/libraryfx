package org.javasoft.util;

import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class MouseDragUtil {

    private double x;
    private double y;

    public void setDragged(Parent parent, Stage stage){
        parent.setOnMousePressed(event -> {
            x=event.getSceneX();
            y=event.getSceneY();
        });

        parent.setOnMouseDragged(event -> {
            stage.setX(event.getScreenX()-x);
            stage.setY(event.getScreenY()-y);
        });
    }
}
