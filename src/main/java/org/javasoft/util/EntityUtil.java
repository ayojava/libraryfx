package org.javasoft.util;

import org.javasoft.entity.BookEntity;
import org.javasoft.entity.MemberEntity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class EntityUtil {

    public List<MemberEntity> buildMemberEntityList(){
        List<MemberEntity> memberEntityList = new ArrayList<>();
        memberEntityList.add(MemberEntity.builder().admin(true).emailAddress("admin@admin.com")
                .firstName("Ayodeji").lastName("Ilori").password("12345")
                .regNo("A001").username("admin1")
                .build());
        memberEntityList.add(MemberEntity.builder().admin(false).emailAddress("user@admin.com")
                .firstName("Lisa").lastName("Jamaine").password("12345")
                .regNo("U001").username("user1")
                .build());
        memberEntityList.add(MemberEntity.builder().admin(false).emailAddress("user@admin.com")
                .firstName("Rick").lastName("Martin").password("12345")
                .regNo("U002").username("user2")
                .build());
        memberEntityList.add(MemberEntity.builder().admin(false).emailAddress("user@admin.com")
                .firstName("Timothy").lastName("Dalton").password("12345")
                .regNo("U003").username("user3")
                .build());
        memberEntityList.add(MemberEntity.builder().admin(false).emailAddress("user@admin.com")
                .firstName("Finn").lastName("Mathew").password("12345")
                .regNo("U004").username("user4")
                .build());
        memberEntityList.add(MemberEntity.builder().admin(true).emailAddress("admin@admin.com")
                .firstName("Gbenga").lastName("Ilori").password("22345")
                .regNo("A002").username("admin2")
                .build());
        return memberEntityList;
    }

    public List<BookEntity> buildBookEntityList(){
        List<BookEntity> bookEntityList = new ArrayList<>();
        bookEntityList.add(BookEntity.builder().bookCode("B001").bookName("Phantom of the Opera").bookISBN("ISBN0123456789").bookAuthor("James Calding").publisherName("Lantern Books")
                .publisherDate(LocalDate.of(2017,12,12))
                .genre("Fantasy").registeredDate(LocalDate.now()).status("AVAILABLE").build());
        bookEntityList.add(BookEntity.builder().bookCode("B002").bookName("Phantom of the Opera").bookISBN("ISBN0123456789").bookAuthor("James Calding").publisherName("Lantern Books")
                .publisherDate(LocalDate.of(2017,12,12))
                .genre("Fantasy").registeredDate(LocalDate.now()).status("AVAILABLE").build());
        bookEntityList.add(BookEntity.builder().bookCode("B003").bookName("Phantom of the Opera").bookISBN("ISBN0123456789").bookAuthor("James Calding").publisherName("Lantern Books")
                .publisherDate(LocalDate.of(2017,12,12))
                .genre("Fantasy").registeredDate(LocalDate.now()).status("AVAILABLE").build());
        bookEntityList.add(BookEntity.builder().bookCode("B004").bookName("Phantom of the Opera").bookISBN("ISBN0123456789").bookAuthor("James Calding").publisherName("Lantern Books")
                .publisherDate(LocalDate.of(2017,12,12))
                .genre("Fantasy").registeredDate(LocalDate.now()).status("AVAILABLE").build());

        bookEntityList.add(BookEntity.builder().bookCode("B005").bookName("The Journey").bookISBN("ISBN7776556789").bookAuthor("Peter Ruddy").publisherName("Verity Books")
                .publisherDate(LocalDate.of(2015,12,10))
                .genre("Fiction").registeredDate(LocalDate.now()).status("AVAILABLE").build());
        bookEntityList.add(BookEntity.builder().bookCode("B006").bookName("The Journey").bookISBN("ISBN7776556789").bookAuthor("Peter Ruddy").publisherName("Verity Books")
                .publisherDate(LocalDate.of(2015,12,10))
                .genre("Fiction").registeredDate(LocalDate.now()).status("AVAILABLE").build());
        bookEntityList.add(BookEntity.builder().bookCode("B007").bookName("The Journey").bookISBN("ISBN7776556789").bookAuthor("Peter Ruddy").publisherName("Verity Books")
                .publisherDate(LocalDate.of(2015,12,10))
                .genre("Fiction").registeredDate(LocalDate.now()).status("AVAILABLE").build());

        bookEntityList.add(BookEntity.builder().bookCode("B008").bookName("The Red Sun").bookISBN("ISBN394833345689").bookAuthor("Rodgers Filli").publisherName("Charity Books")
                .publisherDate(LocalDate.of(2017,3,17))
                .genre("Children").registeredDate(LocalDate.now()).status("AVAILABLE").build());
        bookEntityList.add(BookEntity.builder().bookCode("B009").bookName("The Red Sun").bookISBN("ISBN394833345689").bookAuthor("Rodgers Filli").publisherName("Charity Books")
                .publisherDate(LocalDate.of(2017,3,17))
                .genre("Children").registeredDate(LocalDate.now()).status("AVAILABLE").build());
        bookEntityList.add(BookEntity.builder().bookCode("B010").bookName("The Red Sun").bookISBN("ISBN394833345689").bookAuthor("Rodgers Filli").publisherName("Charity Books")
                .publisherDate(LocalDate.of(2017,3,17))
                .genre("Children").registeredDate(LocalDate.now()).status("AVAILABLE").build());

        return bookEntityList;
    }
}
