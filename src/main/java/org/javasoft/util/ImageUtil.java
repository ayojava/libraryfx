package org.javasoft.util;
import javafx.scene.image.Image;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.time.DateFormatUtils;
import static org.javasoft.constants.ImageIntf.APPLICATION_ICON_PATH;

public class ImageUtil {

    private ImageUtil(){
    }

    private static  ImageUtil imageUtilInstance ;

    public static ImageUtil getImageUtilInstance(){
        if(imageUtilInstance == null){
            imageUtilInstance = new ImageUtil();
        }
        return imageUtilInstance;
    }

    public Image getApplicationIcon(){
        return new Image(getClass().getResourceAsStream(APPLICATION_ICON_PATH));
    }


}
