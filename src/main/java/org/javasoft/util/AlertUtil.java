package org.javasoft.util;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.Getter;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;
import java.util.ResourceBundle;

public class AlertUtil {

    //

    public static void showExceptionDialog(Exception exception){
        Alert alertDialog = new Alert(Alert.AlertType.ERROR);

        alertDialog.setTitle(ResourceBundle.getBundle("libraryfx").getString("dialog.titleLbl"));
        alertDialog.setHeaderText(ResourceBundle.getBundle("libraryfx").getString("dialog.headerText"));

        String content = "Error: ";
        if (exception != null ) {
            content += exception + "\n\n";
        }

        alertDialog.setContentText(content);

        //Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);

        String exceptionText = sw.toString();

        //Set up TextArea
        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setPrefHeight(600);
        textArea.setPrefWidth(800);

        //Set expandable Exception into the dialog pane.
        alertDialog.getDialogPane().setExpandableContent(textArea);
        alertDialog.showAndWait();
    }

    public static void handleExitLabelClicked(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(ResourceBundle.getBundle("libraryfx").getString("dialog.confirmationLbl"));
        alert.setHeaderText(null);
        alert.setContentText(ResourceBundle.getBundle("libraryfx").getString("dialog.confirmationPrompt"));

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            System.exit(0);
        }
    }

    public static Alert showAlert(Alert.AlertType alertType, String title, String header, String text){
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(text);
        alert.showAndWait();
        return alert;
    }

    public static void minimizeClicked(MouseEvent event){
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }
}
