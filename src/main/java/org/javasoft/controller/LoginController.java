package org.javasoft.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.javasoft.service.LoginService;
import org.javasoft.util.AlertUtil;
import org.apache.commons.lang3.*;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    @FXML
    private AnchorPane loginAnchorPane;

    @FXML
    private TextField userNameTxtField;

    @FXML
    private TextField passwordTxtField;

    @FXML
    private Button loginButton;

    @FXML
    private Label exitLabel;

    @FXML
    public void handleExitLabelClicked(){
        AlertUtil.handleExitLabelClicked();
    }

    @FXML
    private void handleExitLabelMouseHover(MouseEvent mouseEvent){
        exitLabel.setStyle("-fx-background-color: red;");
    }

    @FXML
    private void handleExitLabelMouseExit(MouseEvent mouseEvent){
        exitLabel.setStyle("-fx-background-color:  #4183D7;");
    }

    @FXML
    private void handleLogin(ActionEvent actionEvent) throws Exception{
        String usernameTxt = userNameTxtField.getText();
        String passwordTxt = passwordTxtField.getText();
        if(StringUtils.isAnyBlank(usernameTxt,passwordTxt)){
            AlertUtil.showAlert(Alert.AlertType.WARNING,
                    ResourceBundle.getBundle("libraryfx").getString("login.notificationLbl") ,"",
                    ResourceBundle.getBundle("libraryfx").getString("login.messagePrompt")
            );
            userNameTxtField.requestFocus();
        }else{
            LoginService loginService = new LoginService();
            loginService.setUsername(usernameTxt);
            loginService.setPassword(passwordTxt);
            if( ! loginService.handleLogin(actionEvent)){
                AlertUtil.showAlert(Alert.AlertType.ERROR,ResourceBundle.getBundle("libraryfx").getString("login.notificationLbl") ,"",
                        ResourceBundle.getBundle("libraryfx").getString("login.errorPrompt"));
                userNameTxtField.setText(StringUtils.EMPTY);
                passwordTxtField.setText(StringUtils.EMPTY);
                userNameTxtField.requestFocus();
            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
