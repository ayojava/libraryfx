package org.javasoft.controller;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import lombok.Setter;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.javasoft.animation.FadeInRightAnimation;
import org.javasoft.animation.FadeInTransition;
import org.javasoft.animation.FadeOutLeftAnimation;
import org.javasoft.entity.MemberEntity;
import org.javasoft.service.HomeService;
import org.javasoft.util.AlertUtil;


import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.javasoft.constants.ViewIntf.*;

public class HomeController implements Initializable {

    @FXML
    private StackPane transStackPane;

    @FXML
    private AnchorPane blurAnchorPane;

    @FXML
    private AnchorPane rootAnchorPane;

    @FXML
    private Label usernameLbl,emailLbl, timeLbl,jamLbl;

    @FXML
    private Label exitLbl,minimizeLbl;

    @Setter
    private MemberEntity memberEntity;

    private HomeService homeService;

    @FXML
    private ImageView userImageView;

    @FXML
    private VBox operatorVBox;

    @FXML
    private Button homeBtn,listBookBth,listMemberBtn ,listBookShelveBtn;



    @FXML
    public void handleUserImageViewMouseEntered(){
        userImageView.setCursor(Cursor.HAND);
    }

    public void setValueModel(long memberEntityId){

    }

    @FXML
    private void handleUserImageMouseClicked(MouseEvent event) throws IOException{
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource(USER));
        blurAnchorPane.setEffect(new GaussianBlur(10));
        new FadeInRightAnimation(transStackPane).play();
        AnchorPane pane = Loader.load();
//        UserController userController = Loader.getController();
//        userController.setValue(model.getId(), model.getUsername(), model.getPassword(), model.getNama(), model.getEmail());
//        loadPane.getChildren().setAll(pane);
    }

    private void bindToTime() {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(0),
                        actionEvent -> {
                            Calendar time = Calendar.getInstance();
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
                            jamLbl.setText(simpleDateFormat.format(time.getTime()));
                        }
                ),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    public void closeHomeView(ActionEvent actionEvent){
        blurAnchorPane.setEffect(null);
        new FadeOutLeftAnimation(transStackPane).play();
    }

    @FXML
    public void handleLogout(ActionEvent event) throws IOException {
        homeService = new HomeService();
        homeService.handleLogOut(event, usernameLbl.getText());

    }

    @FXML
    public void handleHomeBtnClicked() throws IOException {
        displayHomeMenu();
    }


    @FXML
    private void listBookButtonClicked() throws IOException {
        listBookMenu();
    }

    @FXML
    private void setBackgroundHome(){
        homeBtn.setStyle("-fx-background-color: #D2D7D3");
        listBookBth.setStyle("-fx-background-color: #ECF0F1");

    }

    @FXML
    public void setListBookMenuClicked(MouseEvent mouseEvent){
        listBookBth.setStyle("-fx-background-color: #D2D7D3");
        homeBtn.setStyle("-fx-background-color: #ECF0F1");
        listMemberBtn.setStyle("-fx-background-color: #ECF0F1");
        listBookShelveBtn.setStyle("-fx-background-color: #ECF0F1");
    }

    @FXML
    public void listMemberButtonClicked() throws IOException{
        listMemberMenu();
    }


    @FXML
    public void listBookShelveButtonClicked() throws IOException{
        listBookShelveMenu();
    }


    @FXML
    public void handleExitLabelMouseEntered(MouseEvent mouseEvent){
        exitLbl.setStyle("-fx-background-color: red;");
    }

    @FXML
    public void handleExitClicked(){
        AlertUtil.handleExitLabelClicked();
    }

    @FXML
    public void handleExitLabelMouseExited(MouseEvent mouseEvent){
        exitLbl.setStyle("-fx-background-color:  #4183D7;");
    }

    @FXML
    public void handleMinimizeLabelMouseClicked(MouseEvent mouseEvent){
        AlertUtil.minimizeClicked(mouseEvent);
    }

    @FXML
    public void handleMinimizeLabelMouseEntered(MouseEvent mouseEvent){
        minimizeLbl.setStyle("-fx-background-color: red;");
    }

    @FXML
    private void handleMinimizeLabelMouseExited(javafx.scene.input.MouseEvent event){
        minimizeLbl.setStyle("-fx-background-color:  #4183D7;");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        bindToTime();
        timeLbl.setText(DateFormatUtils.format(new Date(),"dd MMMM yyyy"));
        userImageView.setCursor(Cursor.HAND);
        try {
            displayHomeMenu();
        } catch (IOException ex) {
        }
    }

    private void listBookMenu() throws IOException{
        try {
            rootAnchorPane.getChildren().clear();
            rootAnchorPane.setOpacity(0);
            new FadeInRightAnimation(rootAnchorPane).play();
            AnchorPane pane = FXMLLoader.load(getClass().getResource(LIST_BOOK_VIEW),ResourceBundle.getBundle("libraryfx", new Locale(DEFAULT_LOCALE)));
            rootAnchorPane.getChildren().setAll(pane);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void listMemberMenu() throws IOException{
        try {
            rootAnchorPane.getChildren().clear();
            rootAnchorPane.setOpacity(0);
            new FadeInRightAnimation(rootAnchorPane).play();
            AnchorPane pane = FXMLLoader.load(getClass().getResource(LIST_MEMBER_VIEW),ResourceBundle.getBundle("libraryfx", new Locale(DEFAULT_LOCALE)));
            rootAnchorPane.getChildren().setAll(pane);
        } catch (Exception e) {
            System.out.println(e);
        }
    }


    private void listBookShelveMenu() throws IOException{
        try {
            rootAnchorPane.getChildren().clear();
            rootAnchorPane.setOpacity(0);
            new FadeInRightAnimation(rootAnchorPane).play();
            AnchorPane pane = FXMLLoader.load(getClass().getResource(LIST_BOOK_SHELVE_VIEW),ResourceBundle.getBundle("libraryfx", new Locale(DEFAULT_LOCALE)));
            rootAnchorPane.getChildren().setAll(pane);
        } catch (Exception e) {
            System.out.println(e);
        }
    }


    @FXML
    public void setListMemberMenuClicked(MouseEvent mouseEvent){
        listMemberBtn.setStyle("-fx-background-color: #D2D7D3");
        homeBtn.setStyle("-fx-background-color: #ECF0F1");
        listBookBth.setStyle("-fx-background-color: #ECF0F1");
        listBookShelveBtn.setStyle("-fx-background-color: #ECF0F1");
    }

    @FXML
    public void setListBookShelveMenuClicked(MouseEvent mouseEvent){
        listBookShelveBtn.setStyle("-fx-background-color: #D2D7D3");
        homeBtn.setStyle("-fx-background-color: #ECF0F1");
        listBookBth.setStyle("-fx-background-color: #ECF0F1");
        listMemberBtn.setStyle("-fx-background-color: #ECF0F1");
    }

    private void displayHomeMenu() throws IOException{
        try {
            rootAnchorPane.getChildren().clear();
            rootAnchorPane.setOpacity(0);
            new FadeInRightAnimation(rootAnchorPane).play();
            AnchorPane pane = FXMLLoader.load(getClass().getResource(DASHBOARD),ResourceBundle.getBundle("libraryfx", new Locale(DEFAULT_LOCALE)));
            rootAnchorPane.getChildren().setAll(pane);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
