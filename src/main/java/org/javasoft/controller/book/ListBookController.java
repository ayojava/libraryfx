package org.javasoft.controller.book;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import org.javasoft.animation.FadeInRightAnimation;
import org.javasoft.animation.FadeOutLeftAnimation;
import org.javasoft.download.BookExcelDownload;
import org.javasoft.service.BookService;
import org.javasoft.table.BookTableModel;
import org.javasoft.util.AlertUtil;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import static org.javasoft.constants.ViewIntf.NEW_BOOK_VIEW;

public class ListBookController implements Initializable {

    @FXML
    private StackPane transStackPane;

    @FXML
    private AnchorPane blur;

    @FXML
    private AnchorPane loadAnchorPane;

    @FXML
    private DatePicker datePicker;

    @FXML
    private TextField datePickerTxt;

    @FXML
    private TableView<BookTableModel> listBookTableView;

    @FXML
    private TableColumn<BookTableModel, String> noTableCol,nameTableCol,authorTableCol,isbnTableCol,genreTableCol,publisherDateTableCol,statusTableCol;

    private ObservableList<BookTableModel> observableBookTableModel;

    private BookService bookService ;

    private String num , name , author , isbn , genre , publisherDate ,status;

    @FXML
    ObservableList<String> statusComboBoxLabel = FXCollections.observableArrayList("All","Available","Borrowed");

    @FXML
    private ContextMenu listBookTableContextMenu;

    @FXML
    private ComboBox statusComboBox;

    @FXML
    public void handleListBookTableMouseClickedEvent(MouseEvent mouseEvent)throws IOException{
        if(mouseEvent.getButton() == MouseButton.SECONDARY){
            listBookTableContextMenu.show(listBookTableView,mouseEvent.getScreenX(),mouseEvent.getScreenY());
        }else if(mouseEvent.getClickCount() == 1){
            num = listBookTableView.getSelectionModel().getSelectedItem().getBookId();
            name = listBookTableView.getSelectionModel().getSelectedItem().getBookName();
            author = listBookTableView.getSelectionModel().getSelectedItem().getBookAuthor();
            isbn = listBookTableView.getSelectionModel().getSelectedItem().getBookISBN();
            genre = listBookTableView.getSelectionModel().getSelectedItem().getGenre();
            publisherDate = listBookTableView.getSelectionModel().getSelectedItem().getPublisherDate();
            status = listBookTableView.getSelectionModel().getSelectedItem().getStatus();
        }
    }

    @FXML
    public void deleteBookMenuItem(ActionEvent actionEvent) throws IOException{
        String content = new StringBuilder()
                .append("Name \t\t:").append(name)
                .append("\n Author \t\t:").append(author)
                .append("\n ISBN \t\t:").append(isbn)
                .append("\n Genre \t\t:").append(genre)
                .append("\n PublisherDate \t\t:").append(publisherDate)
                .append("\n Status \t\t:").append(status)
                .toString();
        final Alert deleteBookAlert = AlertUtil.showAlert(Alert.AlertType.CONFIRMATION, "Delete Book", content, "Are you sure you want to delete the book ?");
        Optional<ButtonType> result = deleteBookAlert.showAndWait();
        if (result.get() == ButtonType.OK){
            if(status.equalsIgnoreCase("BORROWED")){
                AlertUtil.showAlert(Alert.AlertType.INFORMATION, "Delete Book", content, "ABook Cannot be deleted because i has been borrowed");
            }else{

            }
        }

    }

    @FXML
    public void closePane(ActionEvent actionEvent){
        loadTable();
        blur.setEffect(null);
        new FadeOutLeftAnimation(transStackPane).play();
        clearParameter();
    }

    @FXML
    public void refreshClicked(){
        loadTable();
    }

    @FXML
    public void datePickerClicked(ActionEvent actionEvent){
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        //datePickerTxt.setText(sdf.format(Date.valueOf(datePicker.getValue())));
    }

    @FXML
    public void newBookBtnClicked() throws IOException {
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource(NEW_BOOK_VIEW));
        blur.setEffect(new GaussianBlur(10));
        new FadeInRightAnimation(transStackPane).play();
        AnchorPane pane = Loader.load();
        loadAnchorPane.getChildren().setAll(pane);
    }


    private void loadTable(){
        bookService = new BookService();
        List<BookTableModel> bookTableModelList = bookService.getBookTableModel(statusComboBox.getSelectionModel().getSelectedItem().toString());
        observableBookTableModel = FXCollections.observableArrayList();
        observableBookTableModel.addAll(bookTableModelList);
        noTableCol.setCellValueFactory(new PropertyValueFactory<>("bookId"));
        nameTableCol.setCellValueFactory(new PropertyValueFactory<>("bookName"));
        authorTableCol.setCellValueFactory(new PropertyValueFactory<>("bookAuthor"));
        isbnTableCol.setCellValueFactory(new PropertyValueFactory<>("bookISBN"));
        genreTableCol.setCellValueFactory(new PropertyValueFactory<>("genre"));
        publisherDateTableCol.setCellValueFactory(new PropertyValueFactory<>("publisherDate"));
        statusTableCol.setCellValueFactory(new PropertyValueFactory<>("status"));
        listBookTableView.setItems(observableBookTableModel);
    }

    private void clearParameter(){
//        id="";
//        detail="";
//        debit="";
//        kredit="";
//        pilih="";
//        tanggal="";
    }

    private void setStatusComboBox(){
        statusComboBox.setValue("--select--");
        statusComboBox.setItems(statusComboBoxLabel);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setStatusComboBox();
        loadTable();
    }

    @FXML
    public void excelDownloadBtnClicked() throws IOException {
        bookService = new BookService();
        BookExcelDownload bookExcelDownload = new BookExcelDownload();
        bookExcelDownload.generateExcelSheet(bookService.getBookEntityList(statusComboBox.getSelectionModel().getSelectedItem().toString()));
    }


}
