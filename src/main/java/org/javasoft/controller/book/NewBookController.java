package org.javasoft.controller.book;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import lombok.val;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.javasoft.entity.BookEntity;
import org.javasoft.service.BookService;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

import static org.javasoft.util.AlertUtil.showAlert;

public class NewBookController implements Initializable {

    @FXML
    private TextField bookNameTextField,authorNameTextField,publisherNameTextField,noOfPagesTextField;

    @FXML
    private DatePicker publishedDatePicker;

    @FXML
    private TextField publishedDateTextField,bookISBNTextField,noOfCopiesTextField;

    @FXML
    private ComboBox genreComboBox;

    @FXML
    public void publishedDatePickerClickedAction(ActionEvent actionEvent){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy");
        String dateText = simpleDateFormat.format(java.sql.Date.valueOf(publishedDatePicker.getValue()));
        publishedDateTextField.setText(dateText);
    }

    @FXML
    public void saveBook(ActionEvent actionEvent){
        if(validateFields()){
            BookEntity bookEntity = BookEntity.builder()
                    .bookCode(RandomStringUtils.randomAlphanumeric(4))
                    .bookAuthor(authorNameTextField.getText())
                    .bookISBN(bookISBNTextField.getText())
                    .bookName(bookNameTextField.getText())
                    .publisherDate(publishedDatePicker.getValue())
                    .publisherName(publisherNameTextField.getText())
                    .registeredDate(LocalDate.now())
                    .genre(genreComboBox.getSelectionModel().getSelectedItem().toString())
                    .build();
            BookService bookService = new BookService();
            if(bookService.saveBook(bookEntity)){
                showAlert(Alert.AlertType.INFORMATION,"Success","","Book Successfully saved");
                clear();
            }else{
                showAlert(Alert.AlertType.ERROR,"Failed","","Book Not saved");
            }
        }

    }

    private boolean validateFields(){
        if (StringUtils.isAnyBlank(authorNameTextField.getText(),bookISBNTextField.getText(),bookNameTextField.getText(),publisherNameTextField.getText(),
                publishedDateTextField.getText())){
            showAlert(Alert.AlertType.WARNING,"","Required Fields","One or More Fields is required");
            return false;
        }
        return true;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        clear();
    }



    private void clear(){
        bookNameTextField.requestFocus();
        authorNameTextField.setText("");
        publisherNameTextField.setText("");
        noOfPagesTextField.setText("");
        bookISBNTextField.setText("");
        noOfCopiesTextField.setText("");
        genreComboBox.setItems(FXCollections.observableArrayList("Bakul","Hotel"));
    }
}
