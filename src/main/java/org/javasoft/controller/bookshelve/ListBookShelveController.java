package org.javasoft.controller.bookshelve;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import lombok.val;
import org.javasoft.animation.FadeOutLeftAnimation;
import org.javasoft.download.BookShelveExcelDownload;
import org.javasoft.service.BookShelveService;
import org.javasoft.table.BookShelveTableModel;
import org.javasoft.table.MemberTableModel;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static org.javasoft.util.AlertUtil.showAlert;

public class ListBookShelveController implements Initializable {

    @FXML
    private StackPane transStackPane;

    private BookShelveService bookShelveService;

    @FXML
    private TextField bookCodeTextField;

    @FXML
    private AnchorPane blur;

    @FXML
    private ContextMenu listBookShelfContextMenu;

    @FXML
    private TableView<BookShelveTableModel> listBookShelveTableView;

    private ObservableList<BookShelveTableModel> observableBookShelveTableModel;

    @FXML
    private TableColumn<BookShelveTableModel, String> noTableCol,bookCodeTableCol,bookNameTableCol,memberNameTableCol,borrowedDateTableCol,dueDateTableCol;

    @FXML
    public void handleListBookShelveTableMouseClickedEvent(MouseEvent mouseEvent)throws IOException {

    }

    @FXML
    public void refreshClicked(){
        loadTable();
    }


    private void loadTable(){
        bookShelveService = new BookShelveService();
        final val bookShelveTableModelList = bookShelveService.getBookShelveTableModel(bookCodeTextField.getText());
        observableBookShelveTableModel = FXCollections.observableArrayList();
        observableBookShelveTableModel.addAll(bookShelveTableModelList);
        noTableCol.setCellValueFactory(new PropertyValueFactory<>("bookShelveId"));
        bookCodeTableCol.setCellValueFactory(new PropertyValueFactory<>("bookCode"));
        bookNameTableCol.setCellValueFactory(new PropertyValueFactory<>("bookName"));
        memberNameTableCol.setCellValueFactory(new PropertyValueFactory<>("memberName"));
        borrowedDateTableCol.setCellValueFactory(new PropertyValueFactory<>("borrowedDate"));
        dueDateTableCol.setCellValueFactory(new PropertyValueFactory<>("dueDate"));
        listBookShelveTableView.setItems(observableBookShelveTableModel);
    }

    @FXML
    public void excelDownloadBtnClicked() throws IOException {
        bookShelveService = new BookShelveService();
        BookShelveExcelDownload bookShelveExcelDownload = new BookShelveExcelDownload();
        bookShelveExcelDownload.generateExcelSheet(bookShelveService.getBookShelveEntityList(bookCodeTextField.getText()));

    }

    @FXML
    public void closePane(ActionEvent actionEvent){
        loadTable();
        blur.setEffect(null);
        new FadeOutLeftAnimation(transStackPane).play();
        clearParameter();
    }

    private void clearParameter(){
        bookCodeTextField.setText("");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadTable();
    }

    @FXML
    public void returnBookMenuItem(ActionEvent actionEvent) throws IOException {
        BookShelveService bookShelveService = new BookShelveService();
        bookShelveService.handleReturnBook(listBookShelveTableView.getSelectionModel().getSelectedItem().getTableId());
        loadTable();
        showAlert(Alert.AlertType.INFORMATION, ResourceBundle.getBundle("libraryfx").getString("dialog.successLbl"),
                "",ResourceBundle.getBundle("libraryfx").getString("dialog.returnBookSuccessful"));

    }
}
