package org.javasoft.controller.member;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import org.apache.commons.lang3.StringUtils;
import org.javasoft.animation.FadeOutLeftAnimation;
import org.javasoft.download.MemberExcelDownload;
import org.javasoft.service.MemberService;
import org.javasoft.table.BookTableModel;
import org.javasoft.table.MemberTableModel;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ListMemberController implements Initializable {

    @FXML
    private StackPane transStackPane;

    private MemberService memberService;


    @FXML
    private TextField statusTextField;

    @FXML
    private AnchorPane blur;

    @FXML
    private ContextMenu listMemberTableContextMenu;

    @FXML
    private TableView<MemberTableModel> listMemberTableView;

    @FXML
    private TableColumn<MemberTableModel, String> noTableCol, firstNameTableCol, lastNameTableCol , userNameTableCol , regNoTableCol ,emailAddressTableCol , adminTableCol;

    @FXML
    ObservableList<String> statusComboBoxLabel = FXCollections.observableArrayList("True","False");

    private ObservableList<MemberTableModel> observableMemberTableModel;

    @FXML
    public void handleListMemberTableMouseClickedEvent(MouseEvent mouseEvent)throws IOException{

    }

    private void loadTable(){
        memberService = new MemberService();
        final List<MemberTableModel> memberTableModelList = memberService.getMemberTableModel(statusTextField.getText());
        observableMemberTableModel = FXCollections.observableArrayList();
        observableMemberTableModel.addAll(memberTableModelList);
        noTableCol.setCellValueFactory(new PropertyValueFactory<>("memberId"));
        firstNameTableCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameTableCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        regNoTableCol.setCellValueFactory(new PropertyValueFactory<>("regNo"));
        emailAddressTableCol.setCellValueFactory(new PropertyValueFactory<>("emailAddress"));
        adminTableCol.setCellValueFactory(new PropertyValueFactory<>("admin"));
        listMemberTableView.setItems(observableMemberTableModel);
    }



    @FXML
    public void closePane(ActionEvent actionEvent){
        loadTable();
        blur.setEffect(null);
        new FadeOutLeftAnimation(transStackPane).play();
        clearParameter();
    }

    private void clearParameter(){

    }

    @FXML
    public void refreshClicked(){
        loadTable();
    }

    @FXML
    public void excelDownloadBtnClicked() throws IOException {
        memberService = new MemberService();
        MemberExcelDownload memberExcelDownload = new MemberExcelDownload();
        memberExcelDownload.generateExcelSheet(memberService.getMemberEntityList(statusTextField.getText()));

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //setStatusComboBox();
        loadTable();
    }


    @FXML
    public void deleteMemberMenuItem(ActionEvent actionEvent) throws IOException{

    }
}
