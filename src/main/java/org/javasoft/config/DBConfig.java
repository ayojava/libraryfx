package org.javasoft.config;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DBConfig {

    private static EntityManagerFactory entityManagerFactory;

    public static synchronized EntityManagerFactory getEntityManagerFactory() {
        if (entityManagerFactory == null) {
            entityManagerFactory = Persistence.createEntityManagerFactory("libraryFxPU");
        }
        return entityManagerFactory;
    }
}
