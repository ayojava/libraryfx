package org.javasoft.table;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MemberTableModel {

    private final StringProperty memberIdStringProperty;

    private final StringProperty firstNameStringProperty;

    private final StringProperty lastNameStringProperty;

    private final StringProperty  regNoStringProperty;

    private final StringProperty emailAddressStringProperty;

    private final StringProperty adminStringProperty;

    public MemberTableModel(String memberId, String firstName , String lastName , String regNo , String emailAddress , String admin){
        this.memberIdStringProperty = new SimpleStringProperty(memberId);
        this.firstNameStringProperty = new SimpleStringProperty(firstName);
        this.lastNameStringProperty = new SimpleStringProperty(lastName);
        this.regNoStringProperty = new SimpleStringProperty(regNo);
        this.emailAddressStringProperty = new SimpleStringProperty(emailAddress);
        this.adminStringProperty = new SimpleStringProperty(admin);
    }

    public String getMemberId(){
        return memberIdStringProperty.get();
    }

    public void setMemberId(String memberId){
        memberIdStringProperty.set(memberId);
    }

    public String getFirstName(){
        return firstNameStringProperty.get();
    }

    public void setFirstName(String firstName){
        firstNameStringProperty.set(firstName);
    }

    public String getLastName(){
        return lastNameStringProperty.get();
    }

    public void setLastName(String lastName){
        lastNameStringProperty.set(lastName);
    }

    public String getRegNo(){
        return regNoStringProperty.get();
    }

    public void setRegNo(String regNo ){
        regNoStringProperty.set(regNo);
    }

    public String getEmailAddress(){
        return emailAddressStringProperty.get();
    }

    public void setEmailAddress(String emailAddress){
        emailAddressStringProperty.set(emailAddress);
    }

    public String getAdmin(){
        return adminStringProperty.get();
    }

    public void setAdmin(String admin){
        adminStringProperty.set(admin);
    }
}
