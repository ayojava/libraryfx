package org.javasoft.table;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class BookTableModel {

    private final StringProperty bookIdStringProperty;

    private  final StringProperty bookNameStringProperty;

    private  final StringProperty bookCodeStringProperty;

    private  final StringProperty bookAuthorStringProperty;

    private  final StringProperty publisherNameStringProperty;

    private  final StringProperty bookISBNStringProperty;

    private  final StringProperty genreStringProperty;

    private final StringProperty publisherDateStringProperty;

    private final StringProperty registeredDateStringProperty;

    private final StringProperty statusStringProperty;


    public BookTableModel(String bookId, String bookName, String bookCode, String bookAuthor, String publisherName,
                          String bookISBN, String genre, String publisherDate, String registeredDate , String status) {
        this.bookIdStringProperty = new SimpleStringProperty(bookId);
        this.bookNameStringProperty = new SimpleStringProperty(bookName);
        this.bookCodeStringProperty = new SimpleStringProperty(bookCode);
        this.bookAuthorStringProperty = new SimpleStringProperty(bookAuthor);
        this.publisherNameStringProperty = new SimpleStringProperty(publisherName);
        this.bookISBNStringProperty = new SimpleStringProperty(bookISBN);
        this.genreStringProperty = new SimpleStringProperty(genre);
        this.publisherDateStringProperty = new SimpleStringProperty(publisherDate);
        this.registeredDateStringProperty = new SimpleStringProperty(registeredDate);
        this.statusStringProperty = new SimpleStringProperty(status);
    }

    public String getStatus(){
        return statusStringProperty.get();
    }

    public void setStatus(String status){
        statusStringProperty.set(status);
    }

    public String getBookCode() {
        return bookCodeStringProperty.get();
    }

    public void setBookCode(String bookCode){
        bookCodeStringProperty.set(bookCode);
    }

    public String getBookAuthor() {
        return bookAuthorStringProperty.get();
    }

    public void setBookAuthor(String bookAuthor){
        bookAuthorStringProperty.set(bookAuthor);
    }

    public String getBookId() {
        return bookIdStringProperty.get();
    }

    public void setBookId(String bookId){
        bookIdStringProperty.set(bookId);
    }

    public String getBookName() {
        return bookNameStringProperty.get();
    }

    public void setBookName(String bookName){
        bookNameStringProperty.set(bookName);
    }

    public String getPublisherDate() {
        return publisherDateStringProperty.get();
    }

    public void setPublisherDate(String publisherDate){
        publisherDateStringProperty.set(publisherDate);
    }

    public String getGenre() {
        return genreStringProperty.get();
    }

    public void setGenre(String genre){
        genreStringProperty.set(genre);
    }

    public String getBookISBN() {
        return bookISBNStringProperty.get();
    }

    public void setBookISBN(String bookISBN){
        bookISBNStringProperty.set(bookISBN);
    }

    public String getRegisteredDate() {
        return registeredDateStringProperty.get();
    }

    public void setRegisteredDate(String registeredDate){
        registeredDateStringProperty.set(registeredDate);
    }

    public String getPublisherName() {
        return publisherNameStringProperty.get();
    }

    public void setPublisherName(String publisherName){
        publisherNameStringProperty.set(publisherName);
    }
}
