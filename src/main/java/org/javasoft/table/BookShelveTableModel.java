package org.javasoft.table;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BookShelveTableModel {

    private final StringProperty bookShelveIdStringProperty;

    private final StringProperty bookNameStringProperty;

    private final StringProperty memberNameStringProperty;

    private final StringProperty borrowedDateStringProperty;

    private final StringProperty dueDateStringProperty;

    private final StringProperty bookCodeStringProperty;

    private final StringProperty tableIdStringProperty;

    public BookShelveTableModel(String bookShelveId, String bookName , String memberName , String borrowedDate , String dueDate , String bookCode, String tableId){
        this.bookShelveIdStringProperty = new SimpleStringProperty(bookShelveId);
        this.bookNameStringProperty = new SimpleStringProperty(bookName);
        this.memberNameStringProperty = new SimpleStringProperty(memberName);
        this.borrowedDateStringProperty = new SimpleStringProperty(borrowedDate);
        this.dueDateStringProperty = new SimpleStringProperty(dueDate);
        this.bookCodeStringProperty = new SimpleStringProperty(bookCode);
        this.tableIdStringProperty = new SimpleStringProperty(tableId);
    }

    public String getTableId(){
        return tableIdStringProperty.get();
    }

    public void setTableId(String tableId){
        tableIdStringProperty.set(tableId);
    }

    public String getBookCode(){
        return bookCodeStringProperty.get();
    }

    public void setBookCode(String bookCode){
        bookCodeStringProperty.set(bookCode);
    }

    public String getBookShelveId(){
        return bookShelveIdStringProperty.get();
    }

    public void setBookShelveId(String bookShelveId){
        bookShelveIdStringProperty.set(bookShelveId);
    }

    public String getBookName(){
        return bookNameStringProperty.get();
    }

    public void setBookName(String bookName){
        bookNameStringProperty.set(bookName);
    }

    public String getMemberName(){
        return memberNameStringProperty.get();
    }

    public void setMemberName(String memberName){
        memberNameStringProperty.set(memberName);
    }

    public String getBorrowedDate(){
        return borrowedDateStringProperty.get();
    }

    public void setBorrowedDate(String borrowedDate ){
        borrowedDateStringProperty.set(borrowedDate);
    }

    public String getDueDate(){
        return dueDateStringProperty.get();
    }

    public void setDueDate(String dueDate){
        dueDateStringProperty.set(dueDate);
    }

}
