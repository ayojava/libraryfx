package org.javasoft.dao;

import org.javasoft.entity.BookEntity;
import org.javasoft.entity.MemberEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BookDao <T extends BookEntity & Serializable> extends BaseDao<T> {

    public BookDao(){
        super(BookDao.class);
    }

    public BookEntity saveBook(BookEntity bookEntity){
        EntityTransaction transaction = null;
        try{
            EntityManager entityManager = getEntityManager();
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.persist(bookEntity);
            entityManager.refresh(bookEntity);
            transaction.commit();
        }catch(Exception ex){
            if(transaction != null){
                transaction.rollback();
            }
            System.out.println("Exception  ::: " + ex);
        }
        finally{
            close();
        }
        return null;
    }

    public List<Object> findAllBooks(){
        try{
            EntityManager entityManager = getEntityManager();
            final Query query = entityManager.createNamedQuery("BookEntity.findAllBooks");
            return  query.getResultList();
        }catch (Exception exception){
            return new ArrayList<>();
        }
    }

    public List<Object> findAllBooksByStatus(String status){
        try{
            EntityManager entityManager = getEntityManager();
            final Query query = entityManager.createNamedQuery("BookEntity.findBooksByStatus");
            query.setParameter("status",status);
            return  query.getResultList();
        }catch (Exception exception){
            return new ArrayList<>();
        }
    }

    public void returnBookEntity(String bookCode){
        EntityTransaction transaction = null;
        try {
            EntityManager entityManager = getEntityManager();
            transaction = entityManager.getTransaction();
            transaction.begin();
            final Query query = entityManager.createNamedQuery("BookEntity.findBookByBookCode");
            query.setParameter("bookCode" ,bookCode);
            final BookEntity bookEntity = (BookEntity) query.getSingleResult();
            bookEntity.setStatus("AVAILABLE");
            entityManager.merge(bookEntity);
            transaction.commit();
        }catch(Exception ex){
            if(transaction != null){
                transaction.rollback();
            }
            System.out.println("Exception  ::: " + ex);
        }
        finally{
            close();
        }
    }

    public BookEntity findBookByBookISBN(String bookISBN){
        try{
            EntityManager entityManager = getEntityManager();
            final Query query = entityManager.createNamedQuery("BookEntity.findBookByBookISBN");
            query.setParameter("bookISBN" ,bookISBN);
            return (BookEntity) query.getSingleResult();
        }catch (Exception exception){
            return null;
        }
    }
}
