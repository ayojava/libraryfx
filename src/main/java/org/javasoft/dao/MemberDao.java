package org.javasoft.dao;

import org.javasoft.entity.MemberEntity;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MemberDao<T extends MemberEntity & Serializable> extends BaseDao<T>  {

    public MemberDao(){
        super(MemberEntity.class);
    }

    public List<Object> findAllMembersByFirstName(String firstName){
        try{
            final EntityManager entityManager = getEntityManager();
            final Query qryObj = entityManager.createNamedQuery("MemberEntity.findByFirstName");
            qryObj.setParameter("firstName","%" + firstName + "%" );
            return  qryObj.getResultList();
        }catch (Exception exception){
            return new ArrayList<>();
        }
    }

    public MemberEntity login(String username , String password){
        EntityTransaction transaction = null;
        try{
            final EntityManager entityManager = getEntityManager();
           // transaction = entityManager.getTransaction();
            final Query qryObj = entityManager.createNamedQuery("MemberEntity.findByUsernameAndPassword");
            qryObj.setParameter("username",username);
            qryObj.setParameter("password",password);
            return (MemberEntity)qryObj.getSingleResult();
            //transaction.begin();
        }catch(Exception ex){
//            if(transaction != null){
//                transaction.rollback();
//            }
            System.out.println("Exception  ::: " + ex);
        }
        finally{
            close();
        }
        return null;
    }

    public List<Object> findAllMembers(){
        try{
            EntityManager entityManager = getEntityManager();
            final Query query = entityManager.createNamedQuery("MemberEntity.findAllMembers");
            return  query.getResultList();
        }catch (Exception exception){
            return new ArrayList<>();
        }
    }
}
