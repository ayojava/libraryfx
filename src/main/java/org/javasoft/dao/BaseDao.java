package org.javasoft.dao;

import javax.persistence.EntityManager;
import java.io.Serializable;

public class BaseDao<T extends Serializable> extends AbstractDao<T>  {

    protected EntityManager entityManager;

    public BaseDao(Class claszz) {
        super(claszz);
    }

    public BaseDao(Class claszz, EntityManager em) {
        super(claszz);
        this.entityManager = em;
    }

    @Override
    public EntityManager getEntityManager() {
        if (entityManager == null || !entityManager.isOpen()) {
            return super.createEntityManager();
        }
        return entityManager;
    }

    public void close() {
        if (entityManager != null && entityManager.isOpen()) {
            entityManager.close();
        }
    }

    public boolean isOpen() {
        return (entityManager != null && entityManager.isOpen());
    }
}
