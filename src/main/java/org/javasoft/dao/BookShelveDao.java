package org.javasoft.dao;

import org.javasoft.entity.BookEntity;
import org.javasoft.entity.BookShelveEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BookShelveDao <T extends BookShelveEntity & Serializable> extends BaseDao<T>{

    public BookShelveDao() {
        super(BookShelveDao.class);
    }

    public List<Object> findAllBookShelves(){
        try{
            EntityManager entityManager = getEntityManager();
            final Query query = entityManager.createNamedQuery("BookShelveEntity.findAllShelves");
            return  query.getResultList();
        }catch (Exception exception){
            return new ArrayList<>();
        }
    }

    public List<Object> findBookByBookCode(String bookCode){
        try{
            EntityManager entityManager = getEntityManager();
            final Query query = entityManager.createNamedQuery("BookShelveEntity.findByBookCode");
            query.setParameter("bookCode","%" + bookCode + "%" );
            return  query.getResultList();

        }catch (Exception exception){
            return new ArrayList<>();
        }
    }

    public BookShelveEntity findByBookShelveId(long bookShelveId){
        try{
            EntityManager entityManager = getEntityManager();
            final Query query = entityManager.createNamedQuery("BookShelveEntity.findById");
            query.setParameter("id" ,bookShelveId);
            return (BookShelveEntity) query.getSingleResult();
        }catch (Exception exception){
            return null;
        }
    }

    public void removeBookShelve(long bookShelveId){
        EntityTransaction transaction = null;
        try{
            EntityManager entityManager = getEntityManager();
            transaction = entityManager.getTransaction();
            transaction.begin();
            final int id = entityManager.createQuery("delete from BookShelveEntity where id = :id")
                    .setParameter("id", bookShelveId)
                    .executeUpdate();
            System.out.println(">>>>>>>>>>" + id);
            transaction.commit();
        }catch (Exception exception){
            System.out.println("Remove failed :::: " + exception);
        }
    }

}
