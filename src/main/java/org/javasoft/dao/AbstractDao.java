package org.javasoft.dao;

import lombok.Getter;
import org.javasoft.config.DBConfig;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

public abstract class AbstractDao<T extends Serializable> implements DaoIntf<T>{

    @Getter
    private Class<T> entityClass;

    public AbstractDao(Class<T> entityClass){
        this.entityClass = entityClass;
    }

    EntityManager createEntityManager() {
        return DBConfig.getEntityManagerFactory().createEntityManager();
    }

    public abstract EntityManager getEntityManager();

    @Override
    public void save(T object) {
        EntityManager entityManager = null;
        try{
            entityManager = getEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(object);
            entityManager.getTransaction().commit();
        }catch (Exception e) {
            if (entityManager != null) {
                entityManager.getTransaction().rollback();
            }
        }finally {
            if(entityManager != null) {
                entityManager.close();
            }
        }
    }

    @Override
    public T  findById(long id) {
        EntityManager entityManager = getEntityManager();
        try {
            return entityManager.find(entityClass, id);
        } catch (Exception e) {
            if ( entityManager != null) {
                entityManager.getTransaction().rollback();
            }
            return null;
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    @Override
    public List<T> findAll() {
        EntityManager entityManager = getEntityManager();
        String queryStr = "select o from " + entityClass.getName() + " as o";
        try {
            Query query = entityManager.createQuery(queryStr);
            return query != null ? query.getResultList() : null;
        }catch (Exception e) {
            if ( entityManager != null) {
                entityManager.getTransaction().rollback();
            }
            return null;
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }
}
