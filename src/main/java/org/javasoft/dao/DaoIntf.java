package org.javasoft.dao;

import java.io.Serializable;
import java.util.List;

public interface DaoIntf<T extends Serializable> {

    void save(T o);

    //void update(String query);

    T findById(long id);

    List<T> findAll();
}
