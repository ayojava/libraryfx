package org.javasoft.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.javasoft.dao.BookDao;
import org.javasoft.dao.BookShelveDao;
import org.javasoft.entity.BookEntity;
import org.javasoft.entity.BookShelveEntity;
import org.javasoft.table.BookShelveTableModel;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class BookShelveService {

    private BookShelveDao bookShelveDao;

    private BookDao bookDao;

    public void handleReturnBook(String bookShelveId){
        bookShelveDao = new BookShelveDao();
        bookDao = new BookDao();
        BookShelveEntity bookShelveEntity = bookShelveDao.findByBookShelveId(Long.valueOf(bookShelveId));
        String  bookCode = bookShelveEntity.getBookEntity().getBookCode();
        bookDao.returnBookEntity(bookCode);
        bookShelveDao.removeBookShelve(Long.valueOf(bookShelveId));
    }

    public List<BookShelveTableModel> getBookShelveTableModel(String bookCode){
        bookShelveDao = new BookShelveDao();
        List<BookShelveTableModel> bookShelveTableModelList = new ArrayList<>();
        int no = 1;
        if(StringUtils.isBlank(bookCode)){
            for(Object bookShelveEntityObj :  bookShelveDao.findAllBookShelves()){
                BookShelveEntity bookShelveEntity = (BookShelveEntity) bookShelveEntityObj;
                BookShelveTableModel bookShelveTableModel =
                        new BookShelveTableModel(String.valueOf(no) ,
                                bookShelveEntity.getBookEntity().getBookName(),
                                bookShelveEntity.getMemberEntity().getFullName(),
                                bookShelveEntity.getBorrowedDate().format(DateTimeFormatter.ofPattern("dd-MMM-yy")),
                                bookShelveEntity.getDueDate().format(DateTimeFormatter.ofPattern("dd-MMM-yy")),
                                bookShelveEntity.getBookEntity().getBookCode(),
                                String.valueOf(bookShelveEntity.getId())
                        );
                bookShelveTableModelList.add(bookShelveTableModel);
                no++;
            }
        }else{
            for(Object bookShelveEntityObj :  bookShelveDao.findBookByBookCode(bookCode)){
                BookShelveEntity bookShelveEntity = (BookShelveEntity) bookShelveEntityObj;
                BookShelveTableModel bookShelveTableModel =
                        new BookShelveTableModel(String.valueOf(no) ,
                                bookShelveEntity.getBookEntity().getBookName(),
                                bookShelveEntity.getMemberEntity().getFullName(),
                                bookShelveEntity.getBorrowedDate().format(DateTimeFormatter.ofPattern("dd-MMM-yy")),
                                bookShelveEntity.getDueDate().format(DateTimeFormatter.ofPattern("dd-MMM-yy")),
                                bookShelveEntity.getBookEntity().getBookCode(),
                                String.valueOf(bookShelveEntity.getId())
                        );
                bookShelveTableModelList.add(bookShelveTableModel);
                no++;
            }
        }
        return bookShelveTableModelList;
    }

    public List<BookShelveEntity>  getBookShelveEntityList(String bookCode){
        bookShelveDao = new BookShelveDao();
        List<Object> bookShelveEntityObjectList = StringUtils.isBlank(bookCode) ?bookShelveDao.findAllBookShelves() :  bookShelveDao.findBookByBookCode(bookCode);

        List<BookShelveEntity> bookShelveEntityList = new ArrayList<>();
        for(Object bookShelveEntityObject :  bookShelveEntityObjectList){
            bookShelveEntityList.add((BookShelveEntity) bookShelveEntityObject);
        }
        return bookShelveEntityList;
    }
}
