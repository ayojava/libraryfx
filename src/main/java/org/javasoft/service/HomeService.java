package org.javasoft.service;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.javasoft.constants.ViewIntf;
import org.javasoft.util.ImageUtil;
import org.javasoft.util.MouseDragUtil;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import static org.javasoft.constants.ViewIntf.DEFAULT_LOCALE;

public class HomeService {

    public void handleLogOut(ActionEvent event, String userName) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(ResourceBundle.getBundle("libraryfx").getString("dialog.logoutLbl"));
        alert.setHeaderText(null);
        alert.setContentText(ResourceBundle.getBundle("libraryfx").getString("dialog.logoutPrompt"));
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            TrayNotification trayNotification = new TrayNotification();
            trayNotification.setNotificationType(NotificationType.CUSTOM);
            trayNotification.setTitle(ResourceBundle.getBundle("libraryfx").getString("dialog.successTitle"));
            trayNotification.setMessage("Bye "+userName+".");
            trayNotification.setAnimationType(AnimationType.FADE);
            trayNotification.showAndDismiss(Duration.millis(1500));
            trayNotification.setRectangleFill(Color.valueOf("#4183D7"));
            //trayNotification.setImage(new Image("/img/icons8_Male_User_100px_2.png"));
            Parent database_parent = FXMLLoader.load(getClass().getResource(ViewIntf.LOGIN),ResourceBundle.getBundle("libraryfx", new Locale(DEFAULT_LOCALE)));

            Scene database_scene = new Scene(database_parent);
            Stage app_stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(database_scene);
            app_stage.setTitle(ResourceBundle.getBundle("libraryfx").getString("dialog.loginLbl"));
            MouseDragUtil mouseDragUtil = new MouseDragUtil();
            mouseDragUtil.setDragged(database_parent, app_stage);
            app_stage.getIcons().add(ImageUtil.getImageUtilInstance().getApplicationIcon());
            app_stage.show();
        }
    }


}
