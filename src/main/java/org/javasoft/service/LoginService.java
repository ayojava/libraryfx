package org.javasoft.service;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import javafx.scene.Parent;
import lombok.Setter;
import org.javasoft.constants.ImageIntf;
import org.javasoft.controller.HomeController;
import org.javasoft.dao.MemberDao;
import org.javasoft.entity.MemberEntity;
import org.javasoft.util.ImageUtil;
import org.javasoft.util.MouseDragUtil;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

import java.util.Locale;
import java.util.ResourceBundle;

import static org.javasoft.constants.ViewIntf.DEFAULT_LOCALE;
import static org.javasoft.constants.ViewIntf.HOME;

public class LoginService {

    @Setter
    private String username;

    @Setter
    private String password;

    private MemberDao memberDao;

    public boolean handleLogin(ActionEvent actionEvent){
        memberDao = new MemberDao();
        MemberEntity memberEntity = memberDao.login(username, password);
        if(memberEntity == null){
            return false;
        }
        handleTrayNotification(memberEntity.getFirstName() + " " + memberEntity.getLastName());
        Stage stage2 = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
        stage2.hide();

        FXMLLoader loader = new FXMLLoader(getClass().getResource(HOME));
        loader.setResources(ResourceBundle.getBundle("libraryfx", new Locale(DEFAULT_LOCALE)));
        try {
            loader.load();
        } catch (Exception e) {
        }

        final HomeController homeController = loader.getController();
        homeController.setMemberEntity(memberEntity);
        //homeController.setValueModel(memberEntity.getId());
        Parent parent = loader.getRoot();
        Stage stage = new Stage();
        Scene parentScene = new Scene(parent);
        parentScene.setFill(javafx.scene.paint.Color.TRANSPARENT);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setScene(parentScene);
        stage.setTitle("Home");

        stage.getIcons().add(ImageUtil.getImageUtilInstance().getApplicationIcon());
        MouseDragUtil mouseDragUtil = new MouseDragUtil();
        mouseDragUtil.setDragged(parent,stage);
        stage.setOnCloseRequest(event -> Platform.runLater(() -> System.exit(0)));
        stage.show();
        return true;
    }

    private void handleTrayNotification(String memberName){
        TrayNotification trayNotification = new TrayNotification();
        trayNotification.setNotificationType(NotificationType.CUSTOM);
        trayNotification.setTitle(ResourceBundle.getBundle("libraryfx").getString("tray.titleLbl"));
        trayNotification.setMessage(ResourceBundle.getBundle("libraryfx").getString("tray.helloLbl") + "     "  + memberName + "     "
                +ResourceBundle.getBundle("libraryfx").getString("tray.welcomeLbl"));
        trayNotification.setAnimationType(AnimationType.FADE);
        trayNotification.showAndDismiss(Duration.millis(1500));
        trayNotification.setRectangleFill(Color.valueOf("#4183D7"));
        //trayNotification.setImage(new Image("/img/icons8_Male_User_100px_2.png"));
    }
}
