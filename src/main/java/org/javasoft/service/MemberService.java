package org.javasoft.service;

import org.apache.commons.lang3.StringUtils;
import org.javasoft.dao.MemberDao;
import org.javasoft.entity.MemberEntity;
import org.javasoft.table.MemberTableModel;

import java.util.ArrayList;
import java.util.List;

public class MemberService {

    private MemberDao memberDao;

    public List<MemberTableModel> getMemberTableModel(String firstNameFlag){
        memberDao =  new MemberDao();
        List<MemberTableModel> memberTableModelList = new ArrayList<>();
        int no = 1;
        if(StringUtils.isNotBlank(firstNameFlag)){
            for(Object aMemberEntityObj : memberDao.findAllMembersByFirstName(firstNameFlag)){
                MemberEntity memberEntity = (MemberEntity) aMemberEntityObj;
                final MemberTableModel memberTableModel = new MemberTableModel(String.valueOf(no), memberEntity.getFirstName(), memberEntity.getLastName(), memberEntity.getRegNo(),
                        memberEntity.getEmailAddress(), String.valueOf(memberEntity.isAdmin()));
                memberTableModelList.add(memberTableModel);
                no++;
            }
        }else{
            for(Object aMemberEntityObj : memberDao.findAllMembers()){
                MemberEntity memberEntity = (MemberEntity) aMemberEntityObj;
                final MemberTableModel memberTableModel = new MemberTableModel(String.valueOf(no), memberEntity.getFirstName(), memberEntity.getLastName(), memberEntity.getRegNo(),
                        memberEntity.getEmailAddress(), String.valueOf(memberEntity.isAdmin()));
                memberTableModelList.add(memberTableModel);
                no++;
            }
        }
        return memberTableModelList;
    }

    public List<MemberEntity> getMemberEntityList(String firstNameFlag){
        memberDao =  new MemberDao();
        List<Object> memberEntityObjectList = (StringUtils.isNotBlank(firstNameFlag))? memberDao.findAllMembersByFirstName(firstNameFlag): memberDao.findAllMembers();
        List<MemberEntity> memberEntityList = new ArrayList<>();
        for(Object aMemberEntityObj : memberEntityObjectList){
            memberEntityList.add((MemberEntity) aMemberEntityObj);
        }
        return memberEntityList;
    }

    public void downloadToExcel(String firstNameFlag){

    }
}
