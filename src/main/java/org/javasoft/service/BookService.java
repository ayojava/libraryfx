package org.javasoft.service;

import org.apache.commons.lang3.StringUtils;
import org.javasoft.dao.BookDao;
import org.javasoft.entity.BookEntity;
import org.javasoft.table.BookTableModel;

import java.util.ArrayList;
import java.util.List;

public class BookService {

    private BookDao bookDao;

    public boolean saveBook(BookEntity bookEntity){
        bookDao = new BookDao();
        if(bookEntity == null){
            return false;
        }
        bookEntity= bookDao.saveBook(bookEntity);
        return bookEntity != null;
    }

    public List<BookTableModel> getBookTableModel(String status){
        bookDao = new BookDao();
        List<BookTableModel> bookTableModelList = new ArrayList<>();
        int i = 1;
        for  (Object bookEntityObj : bookDao.findAllBooks()){
                BookEntity bookEntity = (BookEntity) bookEntityObj;
                if(StringUtils.equalsIgnoreCase(status,"Available") || StringUtils.equalsIgnoreCase(status,"Borrowed")){
                    if(StringUtils.equalsIgnoreCase(bookEntity.getStatus(),status)){
                        BookTableModel bookTableModel = new BookTableModel(String.valueOf(i) , bookEntity.getBookName(), bookEntity.getBookCode(),
                                bookEntity.getBookAuthor(), bookEntity.getPublisherName(), bookEntity.getBookISBN(), bookEntity.getGenre(), bookEntity.getPublisherDate().toString(),
                                bookEntity.getRegisteredDate().toString(), bookEntity.getStatus());
                        bookTableModelList.add(bookTableModel);
                    }
                }else{
                    BookTableModel bookTableModel = new BookTableModel(String.valueOf(i) , bookEntity.getBookName(), bookEntity.getBookCode(),
                            bookEntity.getBookAuthor(), bookEntity.getPublisherName(), bookEntity.getBookISBN(), bookEntity.getGenre(), bookEntity.getPublisherDate().toString(),
                            bookEntity.getRegisteredDate().toString(), bookEntity.getStatus());
                    bookTableModelList.add(bookTableModel);
                }
            i++;
        }
        return bookTableModelList;
    }

    public List<BookEntity> getBookEntityList(String status){
        bookDao = new BookDao();
        List<Object> bookEntityObjectList = null;
        if(StringUtils.equalsIgnoreCase(status,"Available") || StringUtils.equalsIgnoreCase(status,"Borrowed")){
            bookEntityObjectList = bookDao.findAllBooksByStatus(status);
        }else{
            bookEntityObjectList = bookDao.findAllBooks();
        }
        List<BookEntity> bookEntityList = new ArrayList<>();
        for  (Object bookEntityObj : bookEntityObjectList){
            bookEntityList.add((BookEntity)bookEntityObj);
        }
        return bookEntityList;
    }
}
