package org.javasoft.constants;

public interface ViewIntf {

    String LOGIN = "/view/Login.fxml";

    String HOME = "/view/Home.fxml";

    String USER = "/view/User.fxml";

    String DASHBOARD="/view/Dashboard.fxml";

    String LIST_BOOK_VIEW = "/view/book/ListBook.fxml";

    String LIST_MEMBER_VIEW = "/view/member/ListMember.fxml";

    String LIST_BOOK_SHELVE_VIEW = "/view/bookshelve/ListBookShelve.fxml";

    String NEW_BOOK_VIEW ="/view/book/NewBook.fxml";

    String DEFAULT_LOCALE ="en";

   // String DEFAULT_LOCALE ="fi";

}
