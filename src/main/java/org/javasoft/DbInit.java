package org.javasoft;

import lombok.val;
import org.javasoft.dao.MemberDao;
import org.javasoft.entity.MemberEntity;
import org.javasoft.util.EntityUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class DbInit {

    private MemberDao memberDao;

    private EntityUtil entityUtil;

    public DbInit(){
        memberDao = new MemberDao();
        entityUtil = new EntityUtil();
    }

    public void populateDB() {
        EntityTransaction transaction = null;
        try{
            final EntityManager entityManager  = memberDao.getEntityManager();
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityUtil.buildMemberEntityList().forEach(memberEntity -> entityManager.persist(memberEntity));
            entityUtil.buildBookEntityList().forEach(bookEntity -> entityManager.persist(bookEntity));
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw ex;
        } finally {
            memberDao.close();
        }
    }


}
