package org.javasoft.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "book")
@NamedQueries({
        @NamedQuery(name = "BookEntity.findAllBooks" , query = "select b from BookEntity b order by bookName asc "),
        @NamedQuery(name = "BookEntity.findBooksByStatus" , query = "select b from BookEntity b where b.status = :status order by bookName asc "),
        @NamedQuery(name = "BookEntity.findBookByBookCode" , query = "select b from BookEntity b where b.bookCode = :bookCode")
})
public class BookEntity {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private String bookName;

    private String bookCode;

    private String bookAuthor;

    private String publisherName;

    private String bookISBN;

    private String genre;


    private LocalDate publisherDate;


    private LocalDate registeredDate;

    private String status;


}