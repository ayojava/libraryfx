package org.javasoft.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "member")
@NamedQueries({
        @NamedQuery(name = "MemberEntity.findByUsernameAndPassword" ,
                query = "select o from MemberEntity o where o.username=:username and o.password=:password"),
        @NamedQuery(name = "MemberEntity.findAllMembers" , query = "select o from MemberEntity o order by o.firstName asc "),
        @NamedQuery(name = "MemberEntity.findByFirstName" , query = "select o from MemberEntity o where o.firstName like :firstName")
})
public class MemberEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String firstName;

    private String lastName;

    private String username;

    private String password;

    private String regNo;

    @Email
    private String emailAddress;

    private boolean admin;

    @Transient
    public String getFullName(){
        return firstName + "  " + lastName ;
    }
}
