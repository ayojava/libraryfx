package org.javasoft.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "BookShelve")
@NamedQueries({
        @NamedQuery(name = "BookShelveEntity.findAllShelves",
                query = "select o from BookShelveEntity o order by o.borrowedDate asc "),
        @NamedQuery(name = "BookShelveEntity.findByBookCode",
                query = "select o from BookShelveEntity o where o.bookEntity.bookCode like :bookCode "),
        @NamedQuery(name = "BookShelveEntity.findById",
                        query = "select o from BookShelveEntity o where o.id = :id "),
        @NamedQuery(name = "BookShelveEntity.removeById",
                query = "delete from BookShelveEntity b where b.id = :id")
})
public class BookShelveEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @OneToOne
    private BookEntity bookEntity;

    @OneToOne
    private MemberEntity memberEntity;

    private LocalDate borrowedDate;

    private LocalDate dueDate;


}
