module org.javasoft {

    requires javafx.controls;
    requires javafx.fxml;
    requires lombok;
    requires org.apache.commons.lang3;
    requires TrayTester;
    requires com.jfoenix;
    requires org.hibernate.commons.annotations;
    requires java.persistence;
    requires java.validation;
    requires java.sql;
    requires java.xml.bind;
    //requires poi.ooxml;
    requires poi;
    requires poi.ooxml;


    opens org.javasoft.controller to javafx.fxml;
    opens org.javasoft.entity  to org.hibernate.orm.core;
    exports org.javasoft;
    opens org.javasoft.controller.book to javafx.fxml;
    opens org.javasoft.controller.member to javafx.fxml;
    opens org.javasoft.controller.bookshelve to javafx.fxml;
    opens org.javasoft.table to javafx.base;
    opens org.javasoft.download to org.apache.poi.xssf.usermodel;
}